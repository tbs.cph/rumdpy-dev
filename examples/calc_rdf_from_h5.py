""" Minimal example for calculing rdf from existing data """

import os

import rumdpy as rp

file_to_read = "LJ_T0.70.h5" 

if not os.path.isfile(file_to_read):
    print("This example needs a the file LJ_T0.70.h5 to be present")
    print(f"{file_to_read} can be generated using minimal.py example")
    exit()

# Load existing data
output = rp.tools.TrajectoryIO(file_to_read).get_h5()
# Read number of particles N and dimensions from data
nblocks, nconfs, _ , N, D = output['block'].shape

# Create configuration object
configuration = rp.Configuration(D=D, N=N)
configuration.simbox = rp.Orthorhombic(D, output.attrs['simbox_initial'])
configuration.copy_to_device()
# Call the rdf calculator
calc_rdf = rp.CalculatorRadialDistribution(configuration, bins=1000)

# NOTE: the structure of the block is (outer_block, inner_steps, pos&img, npart, dimensions)
#       the zero is to select the position array and discard images
positions = output['block'][:,:,0,:,:]
positions = positions.reshape(nblocks*nconfs,N,D)
# Loop over saved configurations
for pos in positions[nconfs-1::nconfs]:
    configuration['r'] = pos
    configuration.copy_to_device()
    calc_rdf.update()

# Save rdf
calc_rdf.save_average()

